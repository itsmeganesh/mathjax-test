import React from 'react'
import PDFService from './pdf-service'
import {data} from './data'
import './pdf-service/index.css'
const App = () => {
  return (
    <div>
      <PDFService>
      {/* {data.section_list.map(item=>{
        return item.question_list.map(qst=>{
          return <>
          <div className="app" dangerouslySetInnerHTML={{ __html: qst.question_data }}/>
          {qst.option_list.map(opt=><div className="app" dangerouslySetInnerHTML={{ __html: opt.option_data }}/>)}
          </>
        })
      })} */}
      <div dangerouslySetInnerHTML={{ __html: data}}/>

      </PDFService>
    </div>
  )
}

export default App
