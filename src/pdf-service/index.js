import React, { useEffect } from "react";
import { PDFExport, PDFMargin } from "@progress/kendo-react-pdf";
import "./index.css";
import { styles } from "./styles";
function PDFService({ children }) {
  const pdfExportComponent = React.useRef(null);

  const exportPDFWithMethod = () => {
    if (pdfExportComponent.current) {
      pdfExportComponent.current.save();
    }
  };


  return (
    <div className="App">
      <button className="k-button" onClick={exportPDFWithMethod}>
        Export with method
      </button>
      <PDFExport
        ref={pdfExportComponent}
        paperSize="a4"
        margin={40}
        fileName={`Report for ${new Date().getFullYear()}`}
        author="KendoReact Team"
      >
        {/* <style>{styles}</style> */}
        <div id="show_math_loader" style={{ display: "none" }}>
          Parsing Mathjax
        </div>
        <div id="print_page">
          {children}
        </div>
        
      </PDFExport>
   
    </div>
  );
}

export default PDFService;
