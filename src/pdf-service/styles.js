export const styles = `
    /* Use the DejaVu Sans font for displaying and embedding in the PDF file. The standard PDF fonts do not support Unicode characters. */
    div{
        font-family: "KaTeX_Main", "Times New Roman", sans-serif !important;
        font-size: 12px;
    }
  
    @font-face {
        font-family: "KaTeX_Main";
        src: url("http://localhost:3000/fonts/KaTeX_Main-Regular.ttf") format("truetype");
      }
      
      @font-face {
        font-family: "KaTeX_Size1";
        src: url("http://localhost:3000/fonts/KaTeX_Size1-Regular.ttf") format("truetype");
      }
      @font-face {
        font-family: "KaTeX_Size2";
        src: url("http://localhost:3000/fonts/KaTeX_Size2-Regular.ttf") format("truetype");
      }
      @font-face {
        font-family: "KaTeX_Size3";
        src: url("http://localhost:3000/fonts/KaTeX_Size3-Regular.ttf") format("truetype");
      }
      @font-face {
        font-family: "KaTeX_Size4";
        src: url("http://localhost:3000/fonts/KaTeX_Size4-Regular.ttf") format("truetype");
      }
      
      @font-face {
        font-family: "KaTeX_AMS";
        src: url("http://localhost:3000/fonts/KaTeX_AMS-Regular.ttf") format("truetype");
      }
      
      
      @font-face {
        font-family: "KaTeX_Math";
        font-style: italic;
        src: url("http://localhost:3000/fonts/KaTeX_Math-Italic.ttf") format("truetype");
      }
      
      
      @font-face {
        font-family: "KaTeX_Main";
        font-weight: bold;
        src: url("http://localhost:3000/fonts/KaTeX_Main-Bold.ttf") format("truetype");
      }

`;
